import { title } from 'process';
import { Postblog } from './../interfaces/postblog';
import { Observable } from 'rxjs';
import { PostblogService } from './../postblog.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts$;
  title:string;
  body:string;
  panelOpenState = false;
  hasError = false;

  constructor(private route:ActivatedRoute, private PostblogService:PostblogService) { }

  ngOnInit(): void {
    this.posts$=this.PostblogService.getPosts();
    this.posts$.subscribe (
      data => {}
    ,
    error =>{
      this.hasError = true;
    }
  )
}

}


