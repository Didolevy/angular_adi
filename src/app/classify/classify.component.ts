
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {

  selectednetwork;
  networks: string[] = ['bbc', 'cnn', 'nbc'];
  text:string; 
  category:string;

  classify(){
    this.classifyService.classify(this.text).subscribe(
      res => {
        console.log(res);
        this.category =  this.classifyService.categories[res];
        
      }
    )
  }
  
  constructor(private route:ActivatedRoute , private classifyService:ClassifyService) { }

  ngOnInit(): void {
    this.selectednetwork = this.route.snapshot.params.network;

}}
