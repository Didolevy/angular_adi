import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {
  private url ="https://66rsqp9kca.execute-api.us-east-1.amazonaws.com/beta";
  categories:object ={0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'}
  ;

  classify(text:string){
    let json = {'articles':[
      {'text':text}
    ]}
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res);
        let final:string = res.body;  
        console.log(final);
        final = final.replace('[',''); 
        final = final.replace(']','');
        return final; 
      })
    )

  }

  constructor(private http:HttpClient) {

   }
}
