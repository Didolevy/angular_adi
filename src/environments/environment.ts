// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAXqk38f4ZheINERu_r9cABcgLK_uLLG7s",
    authDomain: "hello-33d0c.firebaseapp.com",
    databaseURL: "https://hello-33d0c.firebaseio.com",
    projectId: "hello-33d0c",
    storageBucket: "hello-33d0c.appspot.com",
    messagingSenderId: "165109175666",
    appId: "1:165109175666:web:ff66076878cd27dbfcf9eb",
    measurementId: "G-FDTFPVSWER"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
